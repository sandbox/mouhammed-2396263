<?php
function login_history_views_views_data(){
    $data['login_history'] = array(
        'table' => array(
            'base' => array(
                'title' => t('User login history'),
                'help' => 'TODO: please describe the items in this table!',
            ),
            'group' => 'Login History',
        ),
        'uid' => array(
            'title' => 'Uid',
            'help' => t('uid of user.'),
            'field' => array(
                'handler' => 'views_handler_field_user',
                'click sortable' => TRUE,
            ),
            'argument' => array(
                'handler' => 'views_handler_argument_user_uid',
                'name field' => 'title',
                'numeric' => TRUE
            ),
            'relationship' => array(
                'label' => t('User from Login History'),
                'base' => 'users',
                'base field' => 'uid',
            ),
        ),
        'login' => array(
            'title' => t('Login'),
            'help' => t('Timestamp for user’s login.'),
            'field' => array(
                'handler' => 'views_handler_field_date',
                'click sortable' => TRUE,
            ),
            'sort' => array(
                'handler' => 'views_handler_sort_date',
            ),
        ),
        'hostname' => array(
            'title' => t('Hostname'),
            'help' => t('The user’s host name.'),
            'field' => array(
                'handler' => 'views_handler_field',
                'click sortable' => TRUE,
            ),
        ),
        'one_time' => array(
            'title' => t('One Time'),
            'help' => t('Indicates whether the login was from a one-time login link (e.g. password reset).'),
            'field' => array(
                'handler' => 'views_handler_field_numeric',
                'click sortable' => FALSE,
            ),
        ),
        'user_agent' => array(
            'title' => t('User Agent'),
            'help' => t('User agent (i.e. browser) of the device used during the login.'),
            'field' => array(
                'handler' => 'views_handler_field',
                'click sortable' => FALSE,
            ),
        ),
    );

    return $data;
}