<?php
/**
 * Implements hook_views_default_views().
 */
function login_history_views_views_default_views() {
    $views = array();

    $view = new view();
    $view->name = 'user_login_history';
    $view->description = '';
    $view->tag = 'default';
    $view->base_table = 'login_history';
    $view->human_name = 'User login history';
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

    /* Display: Master */
    $handler = $view->new_display('default', 'Master', 'default');
    $handler->display->display_options['title'] = 'User login history';
    $handler->display->display_options['use_more_always'] = FALSE;
    $handler->display->display_options['access']['type'] = 'none';
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['pager']['type'] = 'full';
    $handler->display->display_options['pager']['options']['items_per_page'] = '10';
    $handler->display->display_options['style_plugin'] = 'table';
    /* Relationship: Login History: Uid */
    $handler->display->display_options['relationships']['uid']['id'] = 'uid';
    $handler->display->display_options['relationships']['uid']['table'] = 'login_history';
    $handler->display->display_options['relationships']['uid']['field'] = 'uid';
    $handler->display->display_options['relationships']['uid']['label'] = 'User';
    $handler->display->display_options['relationships']['uid']['required'] = TRUE;
    /* Field: Login History: Hostname */
    $handler->display->display_options['fields']['hostname']['id'] = 'hostname';
    $handler->display->display_options['fields']['hostname']['table'] = 'login_history';
    $handler->display->display_options['fields']['hostname']['field'] = 'hostname';
    /* Field: Login History: Login */
    $handler->display->display_options['fields']['login']['id'] = 'login';
    $handler->display->display_options['fields']['login']['table'] = 'login_history';
    $handler->display->display_options['fields']['login']['field'] = 'login';
    $handler->display->display_options['fields']['login']['label'] = 'Date';
    $handler->display->display_options['fields']['login']['date_format'] = 'long';
    /* Contextual filter: Login History: Uid */
    $handler->display->display_options['arguments']['uid']['id'] = 'uid';
    $handler->display->display_options['arguments']['uid']['table'] = 'login_history';
    $handler->display->display_options['arguments']['uid']['field'] = 'uid';
    $handler->display->display_options['arguments']['uid']['default_action'] = 'default';
    $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'current_user';
    $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
    $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
    $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
    $handler->display->display_options['arguments']['uid']['specify_validation'] = TRUE;

    /* Display: Page : User own login history */
    $handler = $view->new_display('page', 'Page : User own login history', 'page');
    $handler->display->display_options['defaults']['fields'] = FALSE;
    /* Field: Login History: Hostname */
    $handler->display->display_options['fields']['hostname']['id'] = 'hostname';
    $handler->display->display_options['fields']['hostname']['table'] = 'login_history';
    $handler->display->display_options['fields']['hostname']['field'] = 'hostname';
    /* Field: Login History: Login */
    $handler->display->display_options['fields']['login']['id'] = 'login';
    $handler->display->display_options['fields']['login']['table'] = 'login_history';
    $handler->display->display_options['fields']['login']['field'] = 'login';
    $handler->display->display_options['fields']['login']['label'] = 'Date';
    $handler->display->display_options['fields']['login']['date_format'] = 'long';
    /* Field: Login History: User Agent */
    $handler->display->display_options['fields']['user_agent']['id'] = 'user_agent';
    $handler->display->display_options['fields']['user_agent']['table'] = 'login_history';
    $handler->display->display_options['fields']['user_agent']['field'] = 'user_agent';
    /* Field: Login History: One Time */
    $handler->display->display_options['fields']['one_time']['id'] = 'one_time';
    $handler->display->display_options['fields']['one_time']['table'] = 'login_history';
    $handler->display->display_options['fields']['one_time']['field'] = 'one_time';
    $handler->display->display_options['path'] = 'login-history';

    /* Display: Block : User own login history */
    $handler = $view->new_display('block', 'Block : User own login history', 'block');
    $handler->display->display_options['defaults']['pager'] = FALSE;
    $handler->display->display_options['pager']['type'] = 'some';
    $handler->display->display_options['pager']['options']['items_per_page'] = '5';

    /* Display: Page : All users login history */
    $handler = $view->new_display('page', 'Page : All users login history', 'page_1');
    $handler->display->display_options['defaults']['fields'] = FALSE;
    /* Field: Login History: Hostname */
    $handler->display->display_options['fields']['hostname']['id'] = 'hostname';
    $handler->display->display_options['fields']['hostname']['table'] = 'login_history';
    $handler->display->display_options['fields']['hostname']['field'] = 'hostname';
    /* Field: User: Name */
    $handler->display->display_options['fields']['name']['id'] = 'name';
    $handler->display->display_options['fields']['name']['table'] = 'users';
    $handler->display->display_options['fields']['name']['field'] = 'name';
    $handler->display->display_options['fields']['name']['relationship'] = 'uid';
    /* Field: Login History: Login */
    $handler->display->display_options['fields']['login']['id'] = 'login';
    $handler->display->display_options['fields']['login']['table'] = 'login_history';
    $handler->display->display_options['fields']['login']['field'] = 'login';
    $handler->display->display_options['fields']['login']['label'] = 'Date';
    $handler->display->display_options['fields']['login']['date_format'] = 'long';
    /* Field: Login History: User Agent */
    $handler->display->display_options['fields']['user_agent']['id'] = 'user_agent';
    $handler->display->display_options['fields']['user_agent']['table'] = 'login_history';
    $handler->display->display_options['fields']['user_agent']['field'] = 'user_agent';
    /* Field: Login History: One Time */
    $handler->display->display_options['fields']['one_time']['id'] = 'one_time';
    $handler->display->display_options['fields']['one_time']['table'] = 'login_history';
    $handler->display->display_options['fields']['one_time']['field'] = 'one_time';
    $handler->display->display_options['defaults']['arguments'] = FALSE;
    $handler->display->display_options['path'] = 'all-login-history';
    $views['user_login_history'] = $view;

    return $views;
}
